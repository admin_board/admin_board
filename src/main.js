import { createApp } from 'vue'
import App from './App.vue'
import components from '@/components/UI';
import './style.css';
import './styles/general_authentication.css';
import './styles/Register.css'
import './styles/general.css'
import './styles/Email.css'
import './styles/Lock_screen.css'
import './styles/Confirm.css'
import './styles/Main.css'
import router from './router/router'

import {createYmaps} from "vue-yandex-maps";

const ymaps = createYmaps({
    apikey: '2dce6875-2ec6-42cd-9c32-2badb0c36a52',
})

const app = createApp(App);

components.forEach((component) => {
    app.component(component.name, component)
});

app
    .use(ymaps)
    .use(router)
    .mount('#app')

