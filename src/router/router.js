import {createRouter, createWebHistory} from "vue-router";
import MainPage from "@/pages/Main-page";
import LoginUsers from "@/pages/Authentication/Login-users";
import RegisterUsers from "@/pages/Authentication/Register-users";
import Email_users from "@/pages/Authentication/Email_users";
import Lock_screen_users from "@/pages/Authentication/Lock_screen_users";
import Confirm_users from "@/pages/Authentication/Confirm_users";
import mapsPost from "@/pages/maps-post";
import InformationUsers from "@/pages/Information-users";


const routes = [
    {
        path: '/',
        component: MainPage,
        children:[
            {
                path: '/Login-users',
                component: LoginUsers
            },
            {
                path: '/Register-users',
                component: RegisterUsers
            },
            {
                path: '/Email-users',
                component: Email_users
            },
            {
                path: '/Lock-screen-users',
                component: Lock_screen_users
            },
            {
                path: '/Confirm-users',
                component: Confirm_users
            },
            {
                path: '/maps-post',
                component: mapsPost
            },
            {
                path: '/Information-users',
                component: InformationUsers
            },
        ]
    },


]

const router = createRouter({
    routes,
    history: createWebHistory(process.env.BASE_URL)
})

export default router;